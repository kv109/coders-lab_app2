class User::UserQuery
  def ordered_by_rank
    User.all.sort_by(&:score).reverse
  end
end